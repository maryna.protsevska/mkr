using System;

    public class SMS
    {
        private string number;
        private string txt;
        private TypeSMS type;

        public SMS ()
        {
            number = "";
            txt = "";
            type = TypeSMS.spam;
        }
        public SMS (string n, string t, TypeSMS ty)
        {
            number = n;
            txt = t;
            type = ty;
        }

        public string getNumber()
        {
            return this.number;
        }

        public void setNumber(string number)
        {
            this.number = number;
        }

        public string getTxt()
        {
            return this.txt;
        }

        public void setTxt(string txt)
        {
            this.txt = txt;
        }

        public TypeSMS getType()
        {
            return this.type;
        }

        public void setType(TypeSMS type)
        {
            this.type = type;
        }

        public override string ToString()
        {
            return "Number: {0}\n Text: {1}\n Type sms: {2}\n"+number+txt+type;
        }
    }

