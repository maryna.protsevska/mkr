using Xunit;


public class TestSMSList
{
[Fact]
public void PassingCountSmsByNumberTest()
{
    Assert.Equal(2, SMSList.CountSmsByNumber("0997865222"));
}

[Fact]
public void PassingCountSmsByTypeTest()
{
 Assert.Equal(2, SMSList.CountSmsByType(TypeSMS.family));
}

}