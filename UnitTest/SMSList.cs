﻿using System;

       public class SMSList
    {
        static int n = 3;
        static SMS[] List = new SMS[n];

        static void Main(string[] args)
        {
                     
            for(int i=0;i<n;i++)
            {
                List[i] = new SMS();
            }

            List[0].setNumber("0997865222");
            List[0].setTxt("Hello!");
            List[0].setType(TypeSMS.family);
            
            List[1].setNumber("0448532659");
            List[1].setTxt("You need");
            List[1].setType(TypeSMS.spam);

            List[2].setNumber("0997865222");
            List[2].setTxt("Hi!");
            List[2].setType(TypeSMS.family);
        }

        static public int CountSmsByNumber(string num)
        {
            int count = 0;
            for(int i=0;i<n;i++)
            {
               if(List[i].getNumber() == num)
               {
                count++;
               }
            }
            return count;
        }

        static public int CountSmsByType(TypeSMS ty)
        {
            int count = 0;
            for(int i=0;i<n;i++)
            {
               if(List[i].getType() == ty)
               {
                count++;
               }
            }
            return count;
        }


    }
